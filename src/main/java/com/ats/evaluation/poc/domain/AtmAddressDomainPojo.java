package com.ats.evaluation.poc.domain;

public class AtmAddressDomainPojo {
	public String street;
	public String housenumber;
	public String postalcode;
	public String city;
	public double lat;
	public double lng;
}