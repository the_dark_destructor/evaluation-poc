package com.ats.evaluation.poc.core;

import java.util.List;

import com.ats.evaluation.poc.domain.AtmDomainPojo;
/**
 * Service for searching of ATMs by city or by a full-text search query looking at all the fields of the {@link AtmDomainPojo}
 * @author Andrea Santi
 *
 */
public interface IAtmSearchEngine {
	/**
	 * Returns all the {@link AtmDomainPojo} with the city field matching the city provided in input. The search will be performed in a "contains" fashion, case-insensitive.
	 * @param city the city to search
	 * @return the list of {@link AtmDomainPojo} matching the search 
	 */
	List<AtmDomainPojo> searchByCity(String city);
	/**
	 * Perform a full-text search returning all the {@link AtmDomainPojo} having at least one field matching the search query provided in input.
	 * The search will be performed - for all {@link AtmDomainPojo} fields - in a "contains" fashion, case-insensitive 
	 * @param query the query to search
	 * @return the list of {@link AtmDomainPojo} matching the query 
	 */
	List<AtmDomainPojo> freeTextSearch(String query);
}