package com.ats.evaluation.poc.core;

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.ats.evaluation.poc.domain.AtmDomainPojo;
import com.ats.evaluation.poc.dropbox.externalservice.IAtmRestClient;
import com.ats.evaluation.poc.mappers.IAtmMapper;


/**
 * This class allow to search ATMs retrieved from an external service ({@link IAtmRestClient})
 * @author Andrea Santi
 *
 */
@Component
public class AtmDropboxSearchEngine implements IAtmSearchEngine {

	@Autowired private IAtmRestClient dropboxRestClient;
	
	/**
	 * Create an AtmDropboxSearchEngine that will use the ({@link IAtmRestClient}) provided in input to retrieve the list of ATMs
	 * @param dropboxRestClient
	 */
	public AtmDropboxSearchEngine(IAtmRestClient dropboxRestClient) {
		this.dropboxRestClient = dropboxRestClient;
	}

	@Override
	public List<AtmDomainPojo> searchByCity(String city) {
		List<AtmDomainPojo> returnValue = this.getAtms();

		if (StringUtils.isEmpty(city) == false) {
			returnValue = returnValue
								.stream()
								.filter(Objects::nonNull)
								.filter(buildSearchPredicateByCity(city))
								.collect(Collectors.toList());
		}
		return returnValue;
	}


	@Override
	public List<AtmDomainPojo> freeTextSearch(String query) {
		List<AtmDomainPojo> returnValue = this.getAtms();

		if (StringUtils.isEmpty(query) == false) {
			returnValue = returnValue
								.stream()
								.filter(Objects::nonNull)
								.filter(buildSearchPredicateByStreet(query)
											.or(buildSearchPredicateByHouseNumber(query))
											.or(buildSearchPredicateByPostalCode(query))
											.or(buildSearchPredicateByCity(query))
											.or(buildSearchPredicateByLatitude(query))
											.or(buildSearchPredicateByLongitude(query))
											.or(buildSearchPredicateByDistance(query))
											.or(buildSearchPredicateByType(query))
										)
								.collect(Collectors.toList());
		}
		return returnValue;
	}
	
	private List<AtmDomainPojo> getAtms() {
		return IAtmMapper.INSTANCE.map(this.dropboxRestClient.getAtms());
	}	

	private Predicate<AtmDomainPojo> buildSearchPredicateByStreet(String query) {
		return x -> 
				Objects.nonNull(x.address) && 
				Objects.nonNull(x.address.street) &&
				x.address.street.toLowerCase().contains(query.toLowerCase());	
	}

	private Predicate<AtmDomainPojo> buildSearchPredicateByHouseNumber(String query) {
		return x -> 
			Objects.nonNull(x.address) && 
			Objects.nonNull(x.address.housenumber) &&
			x.address.housenumber.toLowerCase().contains(query.toLowerCase());	
	}

	private Predicate<AtmDomainPojo> buildSearchPredicateByPostalCode(String query) {
		return x -> 
			Objects.nonNull(x.address) && 
			Objects.nonNull(x.address.postalcode) &&
			x.address.postalcode.toLowerCase().contains(query.toLowerCase());	
	}

	private Predicate<AtmDomainPojo> buildSearchPredicateByCity(String query) {
		return x -> 
			Objects.nonNull(x.address) && 
			Objects.nonNull(x.address.city) &&
			x.address.city.toLowerCase().contains(query.toLowerCase());
	}

	private Predicate<AtmDomainPojo> buildSearchPredicateByLatitude(String query) {
		return x -> 
			Objects.nonNull(x.address) && 
			String.valueOf(x.address.lat).toLowerCase().contains(query.toLowerCase());
	}
	
	private Predicate<AtmDomainPojo> buildSearchPredicateByLongitude(String query) {
		return x -> 
			Objects.nonNull(x.address) && 
			String.valueOf(x.address.lng).toLowerCase().contains(query.toLowerCase());
	}

	private Predicate<AtmDomainPojo> buildSearchPredicateByDistance(String query) {
		return x -> 
			Objects.nonNull(x.distance) && 
			String.valueOf(x.distance).toLowerCase().contains(query.toLowerCase());
	}

	private Predicate<AtmDomainPojo> buildSearchPredicateByType(String query) {
		return x -> 
			Objects.nonNull(x.type) && 
			String.valueOf(x.type).toLowerCase().contains(query.toLowerCase());
	}
}