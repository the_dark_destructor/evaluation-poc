package com.ats.evaluation.poc.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ats.evaluation.poc.core.IAtmSearchEngine;
import com.ats.evaluation.poc.domain.AtmDomainPojo;

@RestController
@RequestMapping(value = "/api/v1/atms", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class AtmSearchController {

	@Autowired IAtmSearchEngine atmSearchEngine;
	
	@RequestMapping(params = "query", method = RequestMethod.GET)
	public ResponseEntity<List<AtmDomainPojo>> searchAtmByQuery(@RequestParam("query") String query) {
		return new ResponseEntity<>(this.atmSearchEngine.freeTextSearch(query), HttpStatus.OK);
	}

	@RequestMapping(params = "city", method = RequestMethod.GET)
	public ResponseEntity<List<AtmDomainPojo>> searchAtmByCityName(@RequestParam("city") String city) {
		return new ResponseEntity<>(this.atmSearchEngine.searchByCity(city), HttpStatus.OK);
	}	
}