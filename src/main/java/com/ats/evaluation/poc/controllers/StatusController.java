package com.ats.evaluation.poc.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class StatusController {

	@RequestMapping("/status")
	public ResponseEntity<String> statusCheck() {
		return new ResponseEntity<String>("ok", HttpStatus.OK);
	}
}