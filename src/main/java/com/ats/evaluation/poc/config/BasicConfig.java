package com.ats.evaluation.poc.config;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Configuration
public class BasicConfig {

	@Bean
	public Gson buildGson() {
		GsonBuilder gsonBuilder = new GsonBuilder();
		return gsonBuilder.create();
	}
	
	@Bean
	public CloseableHttpClient buildHttpClient() {
		// Tuning pooling
		PoolingHttpClientConnectionManager connMgr = new PoolingHttpClientConnectionManager();
		connMgr.setMaxTotal(200);
		connMgr.setDefaultMaxPerRoute(200);

		// Tuning timeout
		int connectTimeout = 10000;
		RequestConfig config = RequestConfig
									.custom()
										.setConnectionRequestTimeout(connectTimeout)
										.setConnectTimeout(connectTimeout)
										.setSocketTimeout(connectTimeout)
									.build();

		return HttpClientBuilder
					.create()
						.setDefaultRequestConfig(config)
						.setConnectionManager(connMgr)
					.build();
	}
}