package com.ats.evaluation.poc.exceptions;

public class UnexpectedDropboxRestClientException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public UnexpectedDropboxRestClientException(String msg, Exception ex) {
		super(msg, ex);
	}
}