package com.ats.evaluation.poc.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.ats.evaluation.poc.domain.AtmDomainPojo;
import com.ats.evaluation.poc.dropbox.externalservice.AtmDto;

@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface IAtmMapper {
	
	IAtmMapper INSTANCE = Mappers.getMapper( IAtmMapper.class );

	List<AtmDomainPojo> map(List<AtmDto> atmDtos);

	@Mapping(source = "address.geoLocation.lat", target = "address.lat")
	@Mapping(source = "address.geoLocation.lng", target = "address.lng")
	AtmDomainPojo map(AtmDto atmDto);
}