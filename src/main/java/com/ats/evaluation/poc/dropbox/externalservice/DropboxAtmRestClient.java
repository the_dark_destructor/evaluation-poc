package com.ats.evaluation.poc.dropbox.externalservice;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ats.evaluation.poc.exceptions.UnexpectedDropboxRestClientException;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * Concrete implementation of a Dropbox REST client able to provide ATMs info {@link AtmDto}
 * @author Andrea Santi
 *
 */
@Component
public class DropboxAtmRestClient implements IAtmRestClient {
	
	private static final String SERVICE_BASE_URL = "https://www.dropbox.com/s/6fg0k2wxwrheyqk/ATMs?dl=1";
	private static final Type ATM_DTO_LIST_TYPE = new TypeToken<ArrayList<AtmDto>>(){}.getType();

	@Autowired private CloseableHttpClient httpClient;
	@Autowired private Gson gson;
	
	public DropboxAtmRestClient(CloseableHttpClient httpClient, Gson gson) {
		this.httpClient = httpClient;
		this.gson = gson;
	}

	@Override
	public List<AtmDto> getAtms() {
		List<AtmDto> returnValue = Arrays.asList();
		try {
			HttpGet httpGet = new HttpGet(SERVICE_BASE_URL);
			HttpEntity httpResponse = httpClient.execute(httpGet).getEntity();
			returnValue = gson.fromJson(EntityUtils.toString(httpResponse), ATM_DTO_LIST_TYPE);
			EntityUtils.consume(httpResponse);
		} catch (Exception ex) {
			throw new UnexpectedDropboxRestClientException("Unexpected problem while retrieving the list of ATMs from dropbox service", ex);
		}
		return returnValue;
	}
}