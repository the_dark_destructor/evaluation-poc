package com.ats.evaluation.poc.dropbox.externalservice;

public class AtmDto {
	
	public AtmAddressDto address;
	public double distance;
	public String type;

	public AtmDto() {
		this(null, 0, null);
	}

	public AtmDto(AtmAddressDto address, double distance, String type) {
		this.address = address;
		this.distance = distance;
		this.type = type;
	}
}