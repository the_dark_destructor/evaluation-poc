package com.ats.evaluation.poc.dropbox.externalservice;

import java.util.List;

/**
 * Model a REST service able to provide ATMs information ({@link AtmDto})
 * @author Andrea Santi
 *
 */
public interface IAtmRestClient {
	/**
	 * Provides the list of ATMs known by the service
	 * @return the list of known ATMs
	 */
	List<AtmDto> getAtms();
}