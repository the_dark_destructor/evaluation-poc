package com.ats.evaluation.poc.dropbox.externalservice;

public class AtmGeoLocationDto {
	
	public double lat;
	public double lng;

	public AtmGeoLocationDto() {
		this(0, 0);
	}

	public AtmGeoLocationDto(double lat, double lng) {
		this.lat = lat;
		this.lng = lng;
	}
}