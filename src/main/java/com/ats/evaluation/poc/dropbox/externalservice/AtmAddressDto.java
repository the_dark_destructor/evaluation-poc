package com.ats.evaluation.poc.dropbox.externalservice;

public class AtmAddressDto {
	
	public String street;
	public String housenumber;
	public String postalcode;
	public String city;
	public AtmGeoLocationDto geoLocation;

	public AtmAddressDto(String city) {
		this(null, null, null, city, 0, 0);
	}
	
	public AtmAddressDto() {
		this(null, null, null, null, 0, 0);
	}
	
	public AtmAddressDto(String street, String housenumber, String postalcode, String city, double lat, double lng) {
		this.street = street;
		this.housenumber = housenumber;
		this.postalcode = postalcode;
		this.city = city;
		this.geoLocation = new AtmGeoLocationDto(lat, lng);
	}
}