package com.ats.evaluation.poc;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.ats.evaluation.poc.dropbox.externalservice.IAtmRestClient;
import com.google.gson.Gson;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConfigurationTest {

	@Autowired Gson gson;
	@Autowired IAtmRestClient dropboxRestCliet;

	@Test
	public void contextLoadsWithBeans() {
		assertNotNull(gson);
		assertNotNull(dropboxRestCliet);
	}
}