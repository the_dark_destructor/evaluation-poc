package com.ats.evaluation.poc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StatusControllerTest {
	@Autowired private MockMvc mvc;
	
	@Test
	@WithMockUser(value="dummy")
	public void statusControllerReturnsOkIfUserIsAuthenticated() throws Exception {
		this.mvc
			.perform(get("/api/v1/status"))
			.andExpect(status().isOk()).andExpect(content().string("ok"));
	}
	
	@Test
	public void statusControllerReturnsUnhautorizedIfUserIsNotAuthenticated() throws Exception {
		this.mvc
			.perform(get("/api/v1/status"))
			.andExpect(status().is4xxClientError());
	}
}