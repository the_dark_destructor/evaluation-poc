package com.ats.evaluation.poc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.ats.evaluation.poc.dropbox.externalservice.AtmDto;
import com.ats.evaluation.poc.dropbox.externalservice.IAtmRestClient;
import com.ats.evaluation.poc.mappers.IAtmMapper;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AtmSearchControllerTest {
	@Autowired private MockMvc mvc;
	@MockBean private IAtmRestClient dropboxRestClient;
	@Autowired ObjectMapper objectMapper;
	private List<AtmDto> mockedAtmListResult = Arrays.asList(
			AtmDtoBuilder.build("Via Roma", "123", "61032", "Milan", 1, 2, 3, "mytype"), 
			AtmDtoBuilder.build("Roma"));
	
	@Before
	public void setup() {
		when(dropboxRestClient.getAtms()).thenReturn(mockedAtmListResult);
	}
	
	@Test
	@WithMockUser(value="dummy")
	public void queryByCityReturnsExpectedResultIfUSerIsAuthenticated() throws Exception {
		this.mvc
			.perform(get("/api/v1/atms?city=Roma"))
			.andExpect(status().isOk())
			.andExpect(content()
						.string(
							objectMapper
								.writeValueAsString(
									IAtmMapper.INSTANCE.map(Arrays.asList(AtmDtoBuilder.build("Roma"))) // Convert from dropbox service to domain				
								)
						)
			);
	}
	
	@Test
	public void queryByCityReturnsUnhautorizedIfUserIsNotAuthenticated() throws Exception {
		this.mvc
			.perform(get("/api/v1/atms?city=Roma"))
			.andExpect(status().is4xxClientError());
	}
	
	@Test
	@WithMockUser(value="dummy")
	public void fullTextQueryReturnsExpectedResultIfUSerIsAuthenticated() throws Exception {
		this.mvc
			.perform(get("/api/v1/atms?query=Roma"))
			.andExpect(status().isOk())
			.andExpect(content()
						.string(
							objectMapper
								.writeValueAsString(
									IAtmMapper.INSTANCE.map(mockedAtmListResult) // Convert from dropbox service to domain
								)
						)
			);
	}
	
	@Test
	public void fullTextQueryReturnsUnhautorizedIfUserIsNotAuthenticated() throws Exception {
		this.mvc
			.perform(get("/api/v1/atms?query=Rome"))
			.andExpect(status().is4xxClientError());
	}
}