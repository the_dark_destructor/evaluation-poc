package com.ats.evaluation.poc.core;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.ats.evaluation.poc.AtmDtoBuilder;
import com.ats.evaluation.poc.domain.AtmDomainPojo;
import com.ats.evaluation.poc.dropbox.externalservice.AtmDto;
import com.ats.evaluation.poc.dropbox.externalservice.IAtmRestClient;

@RunWith(MockitoJUnitRunner.class)
public class AtmDropboxSearchEngineTest {
	private static final String CITY_SEARCH_TEXT = "New";
	private static final String FREE_TEXT_SEARCH_TEXT_FOR_STRING = "Dummy";
	private static final String FREE_TEXT_SEARCH_TEXT_FOR_NUMBER = "1";

	private IAtmSearchEngine sut;
	@Mock private IAtmRestClient dropboxRestClient;
	
	@Before
	public void setup() {
		this.sut = new AtmDropboxSearchEngine(dropboxRestClient);
	}
	
	@Test
	public void testSearchByCityRerturnOnlyPojosWithCityAtrributeMatching() {
		
        List<AtmDto> mockedAtmList = Arrays.asList(
        		AtmDtoBuilder.build("New York"),
        		AtmDtoBuilder.build(null),
        		AtmDtoBuilder.build("New Delhi"),
        		AtmDtoBuilder.build("Cairo")
        ); 
		
		when(dropboxRestClient.getAtms()).thenReturn(mockedAtmList);
		
		List<AtmDomainPojo> results = this.sut.searchByCity(CITY_SEARCH_TEXT);
		
		assertThat(results, hasSize(2));
		assertTrue(results.stream().filter(x -> x.address.city.equals("New York")).count() == 1);
		assertTrue(results.stream().filter(x -> x.address.city.equals("New Delhi")).count() == 1);
	}
	
	@Test
	public void testSearchByCityIsCaseInsensitive() {
		
        List<AtmDto> mockedAtmList = Arrays.asList(
        		AtmDtoBuilder.build("nEw York"),
        		AtmDtoBuilder.build(null),
        		AtmDtoBuilder.build("NeW Delhi"),
        		AtmDtoBuilder.build("Cairo")
        ); 
		
		when(dropboxRestClient.getAtms()).thenReturn(mockedAtmList);
		
		List<AtmDomainPojo> results = this.sut.searchByCity(CITY_SEARCH_TEXT);
		
		assertThat(results, hasSize(2));
		assertTrue(results.stream().filter(x -> x.address.city.equals("nEw York")).count() == 1);
		assertTrue(results.stream().filter(x -> x.address.city.equals("NeW Delhi")).count() == 1);
	}
	
	@Test
	public void testFreeTextSearchMatchAllStringFields() {

		List<AtmDto> mockedAtmList = Arrays.asList(
				AtmDtoBuilder.build("street", "number", "postalcode", "city", 1, 2, 3, "type"),
				AtmDtoBuilder.build("dummystreet", "number", "postalcode", "city", 1, 2, 3, "type"),
				AtmDtoBuilder.build("street", "dummynumber", "postalcode", "city", 1, 2, 3, "type"),
				AtmDtoBuilder.build("street", "number", "dummypostalcode", "city", 1, 2, 3, "type"),
				AtmDtoBuilder.build("street", "number", "postalcode", "dummycity", 1, 2, 3, "type"),
				AtmDtoBuilder.build("street", "number", "postalcode", "city", 1, 2, 3, "dummytype")
		); 

		when(dropboxRestClient.getAtms()).thenReturn(mockedAtmList);

		List<AtmDomainPojo> results = this.sut.freeTextSearch(FREE_TEXT_SEARCH_TEXT_FOR_STRING);

		assertThat(results, hasSize(5));
		assertTrue(results.stream().filter(x -> x.address.street.contains("dummystreet")).count() == 1);
		assertTrue(results.stream().filter(x -> x.address.housenumber.contains("dummynumber")).count() == 1);
		assertTrue(results.stream().filter(x -> x.address.postalcode.contains("dummypostalcode")).count() == 1);
		assertTrue(results.stream().filter(x -> x.address.city.contains("dummycity")).count() == 1);
		assertTrue(results.stream().filter(x -> x.type.contains("dummytype")).count() == 1);
	}
	
	@Test
	public void testFreeTextSearchMatchAllDoubleFields() {

		List<AtmDto> mockedAtmList = Arrays.asList(
				AtmDtoBuilder.build("street", "number", "postalcode", "city", 5, 5, 5, "type"),
				AtmDtoBuilder.build("street", "number", "postalcode", "city", 1, 5, 5, "type"),
				AtmDtoBuilder.build("street", "number", "postalcode", "city", 5, 1, 5, "type"),
				AtmDtoBuilder.build("street", "number", "postalcode", "city", 5, 5, 1, "type")
		); 

		when(dropboxRestClient.getAtms()).thenReturn(mockedAtmList);

		List<AtmDomainPojo> results = this.sut.freeTextSearch(FREE_TEXT_SEARCH_TEXT_FOR_NUMBER);

		assertThat(results, hasSize(3));
		assertTrue(results.stream().filter(x -> x.address.lat == 1).count() == 1);
		assertTrue(results.stream().filter(x -> x.address.lng == 1).count() == 1);
		assertTrue(results.stream().filter(x -> x.distance == 1).count() == 1);
	}
}