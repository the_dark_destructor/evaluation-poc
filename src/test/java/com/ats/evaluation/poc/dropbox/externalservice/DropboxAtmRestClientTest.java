package com.ats.evaluation.poc.dropbox.externalservice;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class DropboxAtmRestClientTest {

	private IAtmRestClient sut;
	@Mock(answer = Answers.RETURNS_DEEP_STUBS) private CloseableHttpClient httpClient;
	@Mock private HttpEntity mockedHttpEntity;
	
	@Before
	public void setup() {
		this.sut = new DropboxAtmRestClient(httpClient, new Gson());
	}
	
	@Test
	public void testHttpCLientIsInvoked() throws ClientProtocolException, IOException {
		when(httpClient.execute(any(HttpGet.class)).getEntity()).thenReturn(mockedHttpEntity);
		this.sut.getAtms();
		
		verify(httpClient, times(1)).execute(any(HttpGet.class));
	}
}