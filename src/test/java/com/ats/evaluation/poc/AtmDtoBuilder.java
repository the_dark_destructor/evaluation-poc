package com.ats.evaluation.poc;

import com.ats.evaluation.poc.dropbox.externalservice.AtmAddressDto;
import com.ats.evaluation.poc.dropbox.externalservice.AtmDto;

public class AtmDtoBuilder {

	public static AtmDto build(String city) {
		return new AtmDto(new AtmAddressDto(city), 0, "");
	}
	
	public static AtmDto build(String street, String housenumber, String postalcode, String city, double lat, double lng, double distance, String type) {
		return new AtmDto(new AtmAddressDto(street, housenumber, postalcode, city, lat, lng), distance, type);
	}
}