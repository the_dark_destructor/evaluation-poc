import { AtmAddress } from './atm-address';

export class AtmOverview {
    address: AtmAddress; 
    distance: number;
    type: string;
}