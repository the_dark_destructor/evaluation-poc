export class AtmAddress {
    street: string;
    housenumber: number;
    postalcode: string;
    city: string;
    lat: number;
    lng: number;
}
