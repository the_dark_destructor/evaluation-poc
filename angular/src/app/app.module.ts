import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppComponent } from './app.component';
import { AtmService } from './atm.service';
import { HttpClientModule } from '@angular/common/http';
import { AtmFreeTextSearchComponent } from './atm-free-text-search/atm-free-text-search.component';
import { AtmCityBasedSearchComponent } from './atm-city-based-search/atm-city-based-search.component';
import { AtmSearchResultComponent } from './atm-search-result/atm-search-result.component';

@NgModule({
  declarations: [
    AppComponent,
    AtmFreeTextSearchComponent,
    AtmCityBasedSearchComponent,
    AtmSearchResultComponent,
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: AtmFreeTextSearchComponent },
      { path: 'free-text-search', component: AtmFreeTextSearchComponent },
      { path: 'city-based-search', component: AtmCityBasedSearchComponent },
    ], { useHash: true }),
    HttpClientModule
  ],
  providers: [AtmService],
  bootstrap: [AppComponent]
})
export class AppModule { }