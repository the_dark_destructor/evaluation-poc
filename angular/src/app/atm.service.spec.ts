import { TestBed } from '@angular/core/testing';

import { AtmServiceService } from './atm.service';

describe('AtmServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AtmServiceService = TestBed.get(AtmServiceService);
    expect(service).toBeTruthy();
  });
});
