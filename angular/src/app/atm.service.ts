import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AtmOverview } from './models/atm-overview';

@Injectable({
  providedIn: 'root'
})
export class AtmService {

  private readonly BASE_URL = 'api/v1/atms'

  constructor(private httpClient: HttpClient) {

  }

  searchAtmByCityName(cityName: string): Observable<AtmOverview[]> {
      console.log('searching...' + cityName);
    return this.httpClient.get<AtmOverview[]>(this.BASE_URL + "?city=" + cityName);
  }

  searchAtmByQuery(cityName: string): Observable<AtmOverview[]> {
    return this.httpClient.get<AtmOverview[]>(this.BASE_URL + "?query=" + cityName);
  }
}