import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtmFreeTextSearchComponent } from './atm-free-text-search.component';

describe('AtmFreeTextSearchComponent', () => {
  let component: AtmFreeTextSearchComponent;
  let fixture: ComponentFixture<AtmFreeTextSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtmFreeTextSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtmFreeTextSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
