import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtmSearchResultComponent } from './atm-search-result.component';

describe('AtmSearchResultComponent', () => {
  let component: AtmSearchResultComponent;
  let fixture: ComponentFixture<AtmSearchResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtmSearchResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtmSearchResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
