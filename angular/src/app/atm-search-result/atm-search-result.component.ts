import { Component, OnInit, Input } from '@angular/core';
import { AtmOverview } from '../models/atm-overview';
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'atm-search-result',
    templateUrl: './atm-search-result.component.html',
    styleUrls: ['./atm-search-result.component.scss']
})
export class AtmSearchResultComponent implements OnInit {
    @Input() atms: AtmOverview[] = [];
    @Input() isLoading: boolean;
    @Input() searchText: string;
    faCircleNotch = faCircleNotch;

    constructor() { }

    ngOnInit() {
    }

    shouldDisplayResults(): boolean {
        return this.searchText !== undefined && this.searchText !== '';
    }

    hasResults(): boolean {
        return this.atms !== undefined && this.atms !== null && this.atms.length > 1;
    }
}
