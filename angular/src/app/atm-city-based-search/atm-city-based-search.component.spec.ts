import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtmCityBasedSearchComponent } from './atm-city-based-search.component';

describe('AtmCityBasedSearchComponent', () => {
  let component: AtmCityBasedSearchComponent;
  let fixture: ComponentFixture<AtmCityBasedSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtmCityBasedSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtmCityBasedSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
