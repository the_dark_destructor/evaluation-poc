import { Component, OnInit } from '@angular/core';
import { AtmService } from '../atm.service';
import { BehaviorSubject, Subscription, } from 'rxjs';
import { switchMap, debounceTime, skip, distinctUntilChanged } from 'rxjs/operators';
import { AtmOverview } from '../models/atm-overview';

@Component({
    selector: 'app-atm-city-based-search',
    templateUrl: './atm-city-based-search.component.html',
    styleUrls: ['./atm-city-based-search.component.scss']
})
export class AtmCityBasedSearchComponent implements OnInit {

    private readonly _searchSubject = new BehaviorSubject<string>('');
    private _searchSubscription = Subscription.EMPTY;
    atms: AtmOverview[];
    searchText: string;
    isLoading: boolean;

    constructor(private atmService: AtmService) { }

    ngOnInit() {
        this.isLoading = false;
        this._searchSubscription = this._searchSubject.asObservable()
            .pipe(
                skip(1),
                debounceTime(300),
                distinctUntilChanged(),
                switchMap(value => this.atmService.searchAtmByCityName(value)))
            .subscribe(
                atms => {
                    this.atms = atms;
                    this.isLoading = false;
                },
                error => console.log(error));
    }

    onSearch(value: string): void {
        this.isLoading = true;
        this._searchSubject.next(value);
    }
}